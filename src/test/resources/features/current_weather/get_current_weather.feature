Feature: Weatherstack shall give real-time weather data for given location

  To query the weatherstack API for real-time weather data in a location of your choice, simply attach your preferred
  location to the API's `/current` endpoint as seen in the example request below:
  `http://api.weatherstack.com/current
  ? access_key = YOUR_ACCESS_KEY
  & query = New York`

  Scenario: Correct list of weather data is returned for single query parameter
    Given request query parameter is "Oslo"
    When the user sends GET request to current endpoint
    Then the API response with correct weather data for "Oslo" is returned

  Scenario: Missing query error type is returned for empty query parameter
    Given request query parameter is ""
    When the user sends GET request to current endpoint
    Then the "missing_query" error type is returned
    And the API error code is 601
