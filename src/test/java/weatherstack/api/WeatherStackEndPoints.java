package weatherstack.api;

import weatherstack.api.stepdefs.BaseTest;

public enum WeatherStackEndPoints {
    CURRENT(BaseTest.BASE_URI + "/current?access_key={access_key}&query={query}");

    private final String url;

    WeatherStackEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
